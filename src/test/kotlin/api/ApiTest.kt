package be.simplenotes.c2c.api

import be.simplenotes.c2c.extractors.extractLocale
import be.simplenotes.c2c.extractors.extractRoute
import com.fasterxml.jackson.module.kotlin.readValue
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.http4k.format.Jackson
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ApiTest {
    private val urls = mockk<ApiUrls>()
    private val api = Api(urls)
    private val mapper = Jackson.mapper

    @BeforeEach
    fun beforeEach() {
        clearMocks(urls)
    }

    @ParameterizedTest
    @CsvSource("bavon", "portalet", "test")
    fun parseRoutes(fileName: String) {
        val file = javaClass.getResource("/search/$fileName").readText()
        val (terms, input, output) = file.split("---", limit = 3).map { it.trim() }
        every { urls.search(terms) } returns input
        val results = api.cachedSearch(terms)
        val expectedRoutes = mapper.readValue<List<RouteResult>>(output)
        assertThat(results.routes).containsExactlyInAnyOrderElementsOf(expectedRoutes)
    }

    @ParameterizedTest
    @CsvSource("57103")
    fun route(id: String) {
        val input = javaClass.getResource("/routes/$id-in.json").readText()
        every { urls.getRoute(id) } returns input
        // test
        val root = mapper.readTree(input)
        val route = extractRoute(root)

        root["cooked"].forEach {
            println(it.asText())
        }
        //
    }

    @ParameterizedTest
    @CsvSource("bavon")
    fun outing(id: String) {
        val input = javaClass.getResource("/outings/$id-in.json").readText()
        every { urls.getOuting(id) } returns input
        //
        val json = urls.getOuting(id)
        val root = mapper.readTree(json)

        //

        val locale = extractLocale(root["locales"])
        val doc = mapOf(
                "id" to root["document_id"].asText(null),
                "title" to locale["title"].asText(null),
                "weather" to locale["weather"].asText(null),
                "conditions_levels" to locale["conditions_levels"].asText(null),
                "route_description" to locale["route_description"].asText(null),
                "summary" to locale["summary"].asText(null),
                "version" to locale["version"].asText(null),
                "access_comment" to locale["access_comment"].asText(null),
                "timing" to locale["timing"].asText(null),
                "participants" to locale["participants"].asText(null),
                "hut_comment" to locale["hut_comment"].asText(null),
                "topic_id" to locale["topic_id"].asText(null),
                "description" to locale["description"].asText(null),
                "conditions" to locale["conditions"].asText(null),
        )


        println(doc.entries.joinToString("\n"))
    }
}
