package be.simplenotes.c2c.filters

import org.http4k.core.Filter
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.filter.RequestFilters
import org.slf4j.Logger
import java.io.PrintWriter
import java.io.StringWriter

fun loggingFilter(logger: Logger) = RequestFilters.Tap {
    logger.info("${it.method} ${it.uri}")
}

fun catchAllFilter(logger: Logger) = Filter { next ->
    {
        try {
            next(it)
        } catch (e: Exception) {
            val sw = StringWriter()
            e.printStackTrace(PrintWriter(sw))
            logger.error(sw.toString())
            Response(Status.INTERNAL_SERVER_ERROR)
        }
    }
}
