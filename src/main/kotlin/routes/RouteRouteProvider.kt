package be.simplenotes.c2c.routes

import be.simplenotes.c2c.api.Api
import be.simplenotes.c2c.api.Route
import org.http4k.contract.ContractRoute
import org.http4k.contract.div
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.format.Jackson.auto
import org.http4k.lens.Path

// TODO: rename..
class RouteRouteProvider(private val api: Api) : RouteProvider {

    override fun route(): ContractRoute {
        val responseLens = Body.auto<Route>("Route").toLens()

        val spec = "/route" / Path.of("id", "Route's ID") meta {
            summary = "Route details"
            produces += ContentType.APPLICATION_JSON
        } bindContract Method.GET

        fun route(id: String): HttpHandler = {
            api.getRoute(id)?.let {
                Response(Status.OK).with(responseLens of it)
            } ?: Response(Status.NOT_FOUND)
        }

        return spec to ::route
    }

}
