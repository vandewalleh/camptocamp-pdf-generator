package be.simplenotes.c2c.routes

import be.simplenotes.c2c.templates.render
import com.mitchellbosecke.pebble.PebbleEngine
import org.http4k.contract.ContractRoute
import org.http4k.contract.meta
import org.http4k.core.*

class IndexRouteProvider(private val engine: PebbleEngine) : RouteProvider {

    override fun route(): ContractRoute {
        val spec = "/" meta {
            summary = "Redoc ui"
            produces += ContentType.TEXT_HTML
        } bindContract Method.GET

        val route: HttpHandler = {
            Response(Status.OK).body(engine.render("index")).header("Content-Type", "text/html; charset=utf-8")
        }

        return spec to route
    }

}
