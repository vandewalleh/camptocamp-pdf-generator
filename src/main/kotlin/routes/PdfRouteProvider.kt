package be.simplenotes.c2c.routes

import be.simplenotes.c2c.api.Api
import be.simplenotes.c2c.api.ImageDownloader
import be.simplenotes.c2c.pdf.PdfCreator
import be.simplenotes.c2c.pdf.PreloadedStreamFactory
import org.http4k.contract.ContractRoute
import org.http4k.contract.div
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.lens.Path

class PdfRouteProvider(
        private val api: Api,
        private val pdfCreator: PdfCreator,
        private val imageDownloader: ImageDownloader,
) : RouteProvider {

    override fun route(): ContractRoute {
        val spec = "/route" / Path.of("id", "Route's ID") / "pdf" meta {
            summary = "Generate pdf from routes"
            produces += ContentType("application/pdf")
        } bindContract Method.GET

        fun route(id: String, pdf: String): HttpHandler = {
            api.getRoute(id)?.let {
                val imageMap = imageDownloader.download(it.associations.images)

                Response(Status.OK)
                        .body(pdfCreator.create(it, PreloadedStreamFactory(imageMap)))
                        .header("Content-Type", "application/pdf")
                        .header("Content-Disposition", "attachment; filename=\"out.pdf\"")
            } ?: Response(Status.NOT_FOUND)
        }

        return spec to ::route
    }

}
