package be.simplenotes.c2c.routes

import org.http4k.contract.ContractRoute

interface RouteProvider {
    fun route(): ContractRoute
}
