package be.simplenotes.c2c.routes

import be.simplenotes.c2c.api.Activity
import be.simplenotes.c2c.api.Api
import be.simplenotes.c2c.api.RouteResult
import be.simplenotes.c2c.api.SearchResults
import org.http4k.contract.ContractRoute
import org.http4k.contract.RequestMeta
import org.http4k.contract.meta
import org.http4k.core.*
import org.http4k.format.Jackson.auto
import org.http4k.lens.Query

class SearchRouteProvider(private val api: Api) : RouteProvider {

    override fun route(): ContractRoute {
        val query = Query.required("q", "Search terms")
        val responseLens = Body.auto<SearchResults>("Search results").toLens()

        val spec = "/search" meta {
            summary = "Search routes and waypoints"
            queries += query
            receiving(RequestMeta(Request(Method.GET, "/search").query("q", "tour de bavon")))
            returning(Status.OK, responseLens to SearchResults(listOf(RouteResult(
                    id = "57103",
                    title = "Tour de Bavon : Thor",
                    summary = "Belle et longue voie équipée sur des dalles compactes qui sort sous le sommet. ",
                    activities = listOf(Activity.ROCK_CLIMBING),
            ))))
        } bindContract Method.GET

        val search: HttpHandler = { req: Request ->
            Response(Status.OK).with(responseLens of api.cachedSearch(query(req)))
        }

        return spec to search
    }

}
