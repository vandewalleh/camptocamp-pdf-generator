package be.simplenotes.c2c.api

import be.simplenotes.c2c.extractors.extractRoute
import be.simplenotes.c2c.extractors.extractRoutes
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.benmanes.caffeine.cache.Caffeine
import org.checkerframework.checker.nullness.qual.Nullable
import org.http4k.format.Jackson
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

class Api(private val apiUrls: ApiUrls, private val mapper: ObjectMapper = Jackson.mapper) {
    private val searchCache = Caffeine.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .build<String, SearchResults>()

    fun cachedSearch(terms: String) = searchCache[terms, ::search]!!

    private fun search(terms: String): SearchResults {
        val root = mapper.readTree(apiUrls.search(terms))
        val routes = root["routes"]["documents"].map(::extractRoutes)
        return SearchResults(routes)
    }

    private val routeCache = Caffeine.newBuilder()
            .maximumSize(100)
            .recordStats()
            .expireAfterWrite(15, TimeUnit.MINUTES)
            .build<String, Route>()

    private val logger = LoggerFactory.getLogger("API")

    fun getRoute(id: String): Route? {
        val res = routeCache.get(id) {
            apiUrls.getRoute(it)?.let { json ->
                extractRoute(mapper.readTree(json))
            }
        }
        logger.info("Cache: ${routeCache.stats()}")
        return res
    }

}
