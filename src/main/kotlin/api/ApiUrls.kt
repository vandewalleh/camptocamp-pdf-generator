package be.simplenotes.c2c.api

import org.http4k.core.HttpHandler
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.urlEncoded

class ApiUrls(private val client: HttpHandler) {
    private val baseUrl = "https://api.camptocamp.org"

    private fun get(url: String): String? {
        val res =  client(Request(Method.GET, "$baseUrl$url"))
        return if(res.status.successful) res.bodyString()
        else null
    }
    fun search(terms: String) = get("/search?q=${terms.urlEncoded()}&t=w,r&limit=7")
    fun getRoute(id: String): String? = get("/routes/${id}?cook=fr")
    fun getOuting(id: String) = get("/outings/${id}?cook=fr")
}
