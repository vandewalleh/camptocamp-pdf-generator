package be.simplenotes.c2c.api

import org.http4k.core.HttpHandler
import org.http4k.core.Method
import org.http4k.core.Request
import org.slf4j.LoggerFactory
import java.util.stream.Collectors

class ImageDownloader(private val client: HttpHandler) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun download(images: List<Image>): Map<String, ByteArray?> =
            images.map(Image::medium)
                    .parallelStream()
                    .collect(Collectors.toConcurrentMap(
                            { it },
                            {
                                val res = client(Request(Method.GET, it))
                                logger.info("GET ${res.status} $it")
                                if (res.status.successful) res.body.stream.readAllBytes()
                                else null
                            }
                    ))
}
