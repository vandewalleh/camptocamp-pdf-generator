package be.simplenotes.c2c.api

data class SearchResults(val routes: List<RouteResult>)

data class RouteResult(
        val id: String,
        val title: String,
        val summary: String?,
        val activities: List<Activity>,
)

data class Route(
        val id: String,
        val title: String,
        val summary: String?,
        val routeHistory: String,
        val description: String,
        val remarks: String,
        val gear: String,
        val activities: List<Activity>,
        val height: Height,
        val rating: Rating,
        val associations: Associations,
)

data class Associations(
        val outings: List<Outing>,
        val images: List<Image>,
        val waypoints: List<Waypoint>,
)

data class Image(
        val id: String,
        val square: String,
        val medium: String,
        val big: String,
        val full: String,
        val title: String,
)

data class Outing(
        val id: String,
        val title: String,
        val dateStart: String,
        val dateEnd: String,
        val condition: String?,
        val quality: String,
        val activities: List<Activity>,
)

data class Waypoint(
        val id: String,
        val title: String,
        val elevation: String,
)

data class Height(
        val heightDiffDifficulties: String?,
        val up: String?,
        val down: String?,
        val min: String?,
        val max: String?,
)

data class Rating(
        val global: String?,
        val free: String?,
        val required: String?,
        val engagement: String?,
        val equipmentQuality: String?,
)

enum class Activity {
    SKITOURING,
    SNOW_ICE_MIXED,
    MOUNTAIN_CLIMBING,
    ROCK_CLIMBING,
    ICE_CLIMBING,
    HIKING,
    SNOWSHOEING,
    PARAGLIDING,
    MOUNTAIN_BIKING,
    VIA_FERRATA,
    SLACKLINING,
}
