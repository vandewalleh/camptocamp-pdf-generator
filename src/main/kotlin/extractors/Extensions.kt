package be.simplenotes.c2c.extractors

import be.simplenotes.c2c.api.Activity
import com.fasterxml.jackson.databind.JsonNode

fun JsonNode.activities(): List<Activity> = map {
    Activity.valueOf(it.asText().toUpperCase())
}
