package be.simplenotes.c2c.extractors

import be.simplenotes.c2c.api.*
import com.fasterxml.jackson.databind.JsonNode

fun extractRoutes(node: JsonNode): RouteResult {
    val locale = extractLocale(node["locales"])

    return RouteResult(
            id = node["document_id"].asText(),
            title = locale["title_prefix"].asText() + " : " + locale["title"].asText(),
            summary = locale["summary"].asText(null),
            activities = node["activities"].activities(),
    )
}

fun extractRoute(root: JsonNode): Route {
    val locale = extractLocale(root["locales"])

    return Route(
            root["document_id"].asText(),
            locale["title_prefix"].asText() + " : " + locale["title"].asText(),
            locale["summary"].asText(null),
            locale["route_history"].asText(),
            locale["description"].asText(),
            locale["remarks"].asText(),
            locale["gear"].asText(),
            root["activities"].activities(),
            Height(
                    root["height_diff_difficulties"].asText(null),
                    root["height_diff_up"].asText(null),
                    root["height_diff_down"].asText(null),
                    root["elevation_min"].asText(null),
                    root["elevation_max"].asText(null),
            ),
            Rating(
                    root["global_rating"].asText(null),
                    root["rock_free_rating"].asText(null),
                    root["rock_required_rating"].asText(null),
                    root["engagement_rating"].asText(null),
                    root["equipment_rating"].asText(null),
            ),
            Associations(
                    extractOutings(root["associations"]["recent_outings"]["documents"]),
                    extractImages(root["associations"]["images"]),
                    extractWaypoints(root["associations"]["waypoints"]),
            )
    )

}

fun extractOutings(jsonNode: JsonNode) = jsonNode.map { node ->
    val locale = extractLocale(node["locales"])
    Outing(
            id = node["document_id"].asText(),
            title = locale["title"].asText(),
            dateStart = node["date_start"].asText(),
            dateEnd = node["date_end"].asText(),
            condition = node["condition_rating"].asText(null),
            quality = node["quality"].asText(),
            activities = node["activities"].activities()
    )
}

fun extractImages(jsonNode: JsonNode) = jsonNode.map { node ->
    val locale = extractLocale(node["locales"])
    val filename = node["filename"].asText()
    Image(
            id = node["document_id"].asText(),
            title = locale["title"].asText(),
            square = "https://media.camptocamp.org/c2corg-active/${filename.replace(".", "SI.").replace(".svg", ".jpg")}",
            medium = "https://media.camptocamp.org/c2corg-active/${filename.replace(".", "MI.").replace(".svg", ".jpg")}",
            big = "https://media.camptocamp.org/c2corg-active/${filename.replace(".", "BI.").replace(".svg", ".jpg")}",
            full = "https://media.camptocamp.org/c2corg-active/$filename",
    )
}

fun extractWaypoints(jsonNode: JsonNode) = jsonNode.map { node ->
    val locale = extractLocale(node["locales"])
    Waypoint(
            id = node["document_id"].asText(),
            title = locale["title"].asText(),
            elevation = node["elevation"].asText(),
    )
}
