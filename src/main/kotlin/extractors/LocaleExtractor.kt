package be.simplenotes.c2c.extractors

import com.fasterxml.jackson.databind.JsonNode

fun extractLocale(locales: JsonNode): JsonNode = locales.find { locale ->
    locale["lang"].asText() == "fr"
} ?: locales[0]
