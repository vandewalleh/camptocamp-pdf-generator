package be.simplenotes.c2c.pdf

import be.simplenotes.c2c.api.Route
import be.simplenotes.c2c.templates.render
import com.mitchellbosecke.pebble.template.PebbleTemplate
import com.openhtmltopdf.extend.FSStreamFactory
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder
import com.openhtmltopdf.util.XRLog
import org.jsoup.Jsoup
import org.jsoup.helper.W3CDom
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

class PdfCreator(private val template: PebbleTemplate) {

    fun create(route: Route, streamFactory: FSStreamFactory): ByteArrayInputStream {
        val html = template.render(mapOf("route" to route))

        XRLog.setLoggingEnabled(false)

        val document = W3CDom().fromJsoup(Jsoup.parse(html))
        val out = ByteArrayOutputStream()

        PdfRendererBuilder()
                .withW3cDocument(document, "")
                .useFastMode()
                .toStream(out)
                .useProtocolsStreamImplementation(streamFactory, "http", "https")

                .buildPdfRenderer().apply {
                    layout()
                    createPDF()
                    close()
                }

        out.close()
        return ByteArrayInputStream(out.toByteArray())
    }

}
