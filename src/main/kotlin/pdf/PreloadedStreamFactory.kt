package be.simplenotes.c2c.pdf

import com.openhtmltopdf.extend.FSStream
import com.openhtmltopdf.extend.FSStreamFactory
import java.io.InputStream

class PreloadedStreamFactory(private val images: Map<String, ByteArray?>) : FSStreamFactory {
    override fun getUrl(url: String) = object : FSStream {
        override fun getStream(): InputStream? = images[url]?.inputStream()
        override fun getReader() = null
    }
}
