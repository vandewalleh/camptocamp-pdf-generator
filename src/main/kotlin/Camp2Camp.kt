package be.simplenotes.c2c

import be.simplenotes.c2c.filters.catchAllFilter
import be.simplenotes.c2c.filters.loggingFilter
import be.simplenotes.c2c.routes.RouteProvider
import org.http4k.contract.contract
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.format.Jackson
import org.http4k.server.ServerConfig
import org.http4k.server.asServer
import org.koin.core.context.startKoin
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress

fun main() {
    val koin = startKoin {
        modules(
                serverModule,
                routes,
                clientModule,
                apiModule,
                utilsModule,
                pdfModule,
        )
        properties(mapOf(
                "port" to System.getenv().getOrDefault("PORT", "4000"),
                "host" to System.getenv().getOrDefault("HOST", "localhost"),
        ))
    }.koin

    val appRoutes = koin.getAll<RouteProvider>().map(RouteProvider::route)
    val app = contract {
        renderer = OpenApi3(ApiInfo("Camp2Camp", "1.0-SNAPSHOT"), Jackson)
        descriptionPath = "/api/swagger.json"
        routes.all.addAll(appRoutes)
    }

    val logger = LoggerFactory.getLogger("Camp2Camp")
    val server = koin.get<ServerConfig>()
    val address = koin.get<InetSocketAddress>()

    catchAllFilter(logger)
            .then(ServerFilters.GZip())
            .then(loggingFilter(logger))
            .then(app)
            .asServer(server)
            .start()

    logger.info("Listening on http://${address.hostName}:${address.port}")
}
