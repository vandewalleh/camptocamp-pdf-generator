package be.simplenotes.c2c.templates

import com.mitchellbosecke.pebble.PebbleEngine
import com.mitchellbosecke.pebble.cache.tag.CaffeineTagCache
import com.mitchellbosecke.pebble.cache.template.CaffeineTemplateCache
import com.mitchellbosecke.pebble.loader.ClasspathLoader
import com.mitchellbosecke.pebble.template.PebbleTemplate
import java.io.StringWriter

fun PebbleEngine.render(name: String, args: Map<String, Any?> = mapOf()) = getTemplate(name).render(args)

fun PebbleTemplate.render(args: Map<String, Any?> = mapOf()): String {
    val writer = StringWriter()
    evaluate(writer, args)
    return writer.toString()
}

@Suppress("FunctionName")
fun PebbleEngine(cache: Boolean, prefix: String? = null, suffix: String? = ".twig"): PebbleEngine =
        PebbleEngine.Builder().loader(ClasspathLoader().also { loader ->
            prefix?.let { loader.prefix = it }
            suffix?.let { loader.suffix = it }
        })
                .cacheActive(cache)
                .templateCache(CaffeineTemplateCache())
                .tagCache(CaffeineTagCache())
                .build()
