package be.simplenotes.c2c.server

import org.apache.hc.core5.http.impl.bootstrap.ServerBootstrap
import org.apache.hc.core5.http.io.SocketConfig
import org.http4k.core.HttpHandler
import org.http4k.server.Http4kRequestHandler
import org.http4k.server.Http4kServer
import org.http4k.server.ServerConfig
import java.net.InetSocketAddress

class CustomApacheServer(private val socketAddress: InetSocketAddress) : ServerConfig {

    override fun toServer(httpHandler: HttpHandler): Http4kServer = object : Http4kServer {
        val handler = Http4kRequestHandler(httpHandler)

        val server = ServerBootstrap.bootstrap()
                .setListenerPort(socketAddress.port)
                .setLocalAddress(socketAddress.address)
                .setSocketConfig(SocketConfig.custom()
                        .setTcpNoDelay(true)
                        .setSoKeepAlive(true)
                        .setSoReuseAddress(true)
                        .setBacklogSize(1000)
                        .build())
                .apply {
                    register("*", handler)
                    registerVirtual("dev.simplenotes.be", "*", handler) // TODO: find a way to put a wildcard
                }.create()

        override fun start() = apply { server.start() }

        override fun stop() = apply { server.stop() }

        override fun port(): Int = socketAddress.port
    }
}
