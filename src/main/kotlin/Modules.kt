package be.simplenotes.c2c

import be.simplenotes.c2c.api.Api
import be.simplenotes.c2c.api.ApiUrls
import be.simplenotes.c2c.api.ImageDownloader
import be.simplenotes.c2c.pdf.PdfCreator
import be.simplenotes.c2c.routes.*
import be.simplenotes.c2c.server.CustomApacheServer
import be.simplenotes.c2c.templates.PebbleEngine
import com.mitchellbosecke.pebble.PebbleEngine
import org.apache.hc.client5.http.impl.DefaultRedirectStrategy
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder
import org.http4k.client.ApacheClient
import org.http4k.format.Jackson
import org.http4k.server.ServerConfig
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import java.net.InetSocketAddress

val routes = module {
    single { SearchRouteProvider(get()) } bind RouteProvider::class
    single { RouteRouteProvider(get()) } bind RouteProvider::class
    single { PdfRouteProvider(get(), get(), get()) } bind RouteProvider::class
    single { IndexRouteProvider(get()) } bind RouteProvider::class
}

val serverModule = module {
    single { InetSocketAddress(getProperty("host"), getProperty("port").toInt()) }
    single<ServerConfig> { CustomApacheServer(get()) }
}

val clientModule = module {
    single { ApacheClient(get()) }
    single { HttpClientBuilder.create().setRedirectStrategy(DefaultRedirectStrategy()).build() }
}

val apiModule = module {
    single { ApiUrls(get()) }
    single { Api(get(), get()) }
    single { ImageDownloader(get()) }
}

val utilsModule = module {
    single { Jackson.mapper }
    single { PebbleEngine(cache = true, prefix = "templates/") }
}

val pdfModule = module {
    val pdf = named("pdf")
    single { PdfCreator(get(pdf)) }
    single(pdf) { get<PebbleEngine>().getTemplate("pdf") }
}
