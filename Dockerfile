FROM openjdk:14-alpine as jdkbuilder
RUN apk add --no-cache binutils
ENV MODULES java.base,java.compiler,java.desktop,java.logging,java.management,java.naming,java.security.jgss,java.xml,jdk.crypto.ec,jdk.unsupported
RUN jlink --output /myjdk --module-path $JAVA_HOME/jmods --add-modules $MODULES --no-header-files --no-man-pages --strip-debug --compress=2
RUN strip -p --strip-unneeded /myjdk/lib/server/libjvm.so

FROM maven:3.6.3-jdk-14 as builder
WORKDIR /app
COPY pom.xml .
RUN mvn verify clean --fail-never
COPY src/main src/main
RUN mvn package

FROM alpine
ENV APPLICATION_USER app
RUN adduser -D -g '' $APPLICATION_USER
RUN mkdir /app
RUN chown -R $APPLICATION_USER /app
USER $APPLICATION_USER
COPY --from=builder /app/target/c2c*.jar /app/app.jar
COPY --from=jdkbuilder /myjdk /myjdk
WORKDIR /app
EXPOSE 4000
CMD ["/myjdk/bin/java", "-server", "-XX:+UseG1GC", "-XX:+UseStringDeduplication", "-jar", "app.jar"]
